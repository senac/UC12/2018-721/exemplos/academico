-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: academico
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aluno`
--

DROP TABLE IF EXISTS `aluno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aluno` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(200) NOT NULL,
  `sobrenome` varchar(200) NOT NULL,
  `IDADE` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aluno`
--

LOCK TABLES `aluno` WRITE;
/*!40000 ALTER TABLE `aluno` DISABLE KEYS */;
INSERT INTO `aluno` VALUES (1,'Daniel','Santiago',36),(2,'Jose','Silva',37),(3,'Miguel','Santos',40),(4,'Jonas','Gonçalves',25),(5,'Renato','Silva',55),(6,'Sofia','Araujo',37),(7,'Andre','Braz',36),(8,'Ana','Rangel',40),(9,'Ananias','Santos',60),(10,'Jose Braz','Araujo',NULL),(11,'Andreia Bernardes','Pacheco',NULL),(12,'Jose','Nazare',NULL);
/*!40000 ALTER TABLE `aluno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `boletim`
--

DROP TABLE IF EXISTS `boletim`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `boletim` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_ALUNO` int(11) NOT NULL,
  `NOTA` float DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `boletim`
--

LOCK TABLES `boletim` WRITE;
/*!40000 ALTER TABLE `boletim` DISABLE KEYS */;
INSERT INTO `boletim` VALUES (1,1,10),(2,1,10),(3,1,9.5),(4,2,5.5),(5,2,6),(6,2,7),(7,3,5),(8,3,3),(9,3,7),(10,4,8),(11,4,9),(12,4,9),(13,5,2.5),(14,5,3.6),(15,5,10),(16,6,4.5),(17,6,9),(18,6,7),(19,7,1),(20,7,8),(21,7,8),(22,8,9),(23,8,9),(24,8,9),(25,9,6),(26,9,3),(27,9,7),(28,10,8),(29,10,7),(30,10,8),(31,11,9),(32,11,7),(33,11,1),(34,12,6),(35,12,6),(36,12,6),(37,13,7.7),(38,13,9),(39,13,6);
/*!40000 ALTER TABLE `boletim` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `disciplina`
--

DROP TABLE IF EXISTS `disciplina`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disciplina` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Codigo` varchar(5) NOT NULL,
  `Descricao` varchar(200) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `disciplina`
--

LOCK TABLES `disciplina` WRITE;
/*!40000 ALTER TABLE `disciplina` DISABLE KEYS */;
INSERT INTO `disciplina` VALUES (1,'UC01','Planejar e executar a montagem de computadores'),(2,'UC02','Planejar e executar a instalação de hardware e software para computadores'),(3,'UC03','Planejar e executar a manutenção de computadores'),(4,'UC04','Projeto Integrador Assistente de Suporte e Manutenção de Computadores'),(5,'UC05','Planejar e executar a instalação de redes locais de computadores'),(6,'UC06','Planejar e executar a manutenção de redes locais de computadores'),(7,'UC07','Planejar e executar a instalação, a configuração e o monitoramento de sistemas operacionais de redes locais (servidores)'),(8,'UC08','Projeto Integrador Assistente de Operação de Redes de Computadores'),(9,'UC09','Conceber, analisar e planejar o desenvolvimento de software'),(10,'UC10','Executar os processos de codificação, manutenção e documentação de aplicativos computacionais para desktops'),(11,'UC11','Executar os processos de codificação, manutenção e documentação de aplicativos computacionais para dispositivos móveis'),(12,'UC12','Executar os processos de codificação, manutenção e documentação de aplicativos computacionais para internet'),(13,'UC13','Executar teste e implantação de aplicativos computacionais'),(14,'UC14','Desenvolver e organizar elementos estruturais de sites'),(15,'UC15','Manipular e otimizar imagens vetoriais, bitmaps gráficos e elementos visuais de navegação para web'),(16,'UC16','Projeto Integrador Assistente de Desenvolvimento de Aplicativos Computacionais'),(17,'UC01','Planejar e executar a montagem de computadores'),(18,'UC02','Planejar e executar a instalação de hardware e software para computadores'),(19,'UC03','Planejar e executar a manutenção de computadores'),(20,'UC04','Projeto Integrador Assistente de Suporte e Manutenção de Computadores'),(21,'UC05','Planejar e executar a instalação de redes locais de computadores'),(22,'UC06','Planejar e executar a manutenção de redes locais de computadores'),(23,'UC07','Planejar e executar a instalação, a configuração e o monitoramento de sistemas operacionais de redes locais (servidores)'),(24,'UC08','Projeto Integrador Assistente de Operação de Redes de Computadores'),(25,'UC09','Conceber, analisar e planejar o desenvolvimento de software'),(26,'UC10','Executar os processos de codificação, manutenção e documentação de aplicativos computacionais para desktops'),(27,'UC11','Executar os processos de codificação, manutenção e documentação de aplicativos computacionais para dispositivos móveis'),(28,'UC12','Executar os processos de codificação, manutenção e documentação de aplicativos computacionais para internet'),(29,'UC13','Executar teste e implantação de aplicativos computacionais'),(30,'UC14','Desenvolver e organizar elementos estruturais de sites'),(31,'UC15','Manipular e otimizar imagens vetoriais, bitmaps gráficos e elementos visuais de navegação para web'),(32,'UC16','Projeto Integrador Assistente de Desenvolvimento de Aplicativos Computacionais');
/*!40000 ALTER TABLE `disciplina` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professor`
--

DROP TABLE IF EXISTS `professor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professor` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(200) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professor`
--

LOCK TABLES `professor` WRITE;
/*!40000 ALTER TABLE `professor` DISABLE KEYS */;
INSERT INTO `professor` VALUES (1,'Daniel Santiago'),(2,'Thiago Portugal'),(3,'Marcelo Ferreira'),(4,'Marcia Fortuna'),(5,'Jonatan Silva'),(6,'Patrick Cansado');
/*!40000 ALTER TABLE `professor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professor_disciplina`
--

DROP TABLE IF EXISTS `professor_disciplina`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professor_disciplina` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_PROFESSOR` int(11) NOT NULL,
  `ID_DISCIPLINA` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professor_disciplina`
--

LOCK TABLES `professor_disciplina` WRITE;
/*!40000 ALTER TABLE `professor_disciplina` DISABLE KEYS */;
INSERT INTO `professor_disciplina` VALUES (1,3,1),(2,3,2),(3,3,3),(4,5,4),(5,4,5),(6,2,6),(7,2,7),(8,2,8),(9,2,9),(10,1,10),(11,1,11),(12,1,12),(13,1,13),(14,6,14),(15,1,15),(16,1,16),(17,3,1),(18,3,2),(19,3,3),(20,5,4),(21,4,5),(22,2,6),(23,2,7),(24,2,8),(25,2,9),(26,1,10),(27,1,11),(28,1,12),(29,1,13),(30,6,14),(31,1,15),(32,1,16);
/*!40000 ALTER TABLE `professor_disciplina` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'academico'
--

--
-- Dumping routines for database 'academico'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-13 20:30:12
