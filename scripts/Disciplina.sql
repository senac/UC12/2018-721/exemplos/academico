CREATE TABLE IF NOT EXISTS Disciplina(
ID int not null auto_increment primary key  , 
Codigo varchar(5) not null ,
Descricao varchar (200) not null 
) ; 

INSERT INTO Disciplina(Codigo , Descricao) values ('UC01' , 'Planejar e executar a montagem de computadores');
INSERT INTO Disciplina(Codigo , Descricao) values ('UC02' , 'Planejar e executar a instalação de hardware e software para computadores');
INSERT INTO Disciplina(Codigo , Descricao) values ('UC03' , 'Planejar e executar a manutenção de computadores');
INSERT INTO Disciplina(Codigo , Descricao) values ('UC04' , 'Projeto Integrador Assistente de Suporte e Manutenção de Computadores');
INSERT INTO Disciplina(Codigo , Descricao) values ('UC05' , 'Planejar e executar a instalação de redes locais de computadores');
INSERT INTO Disciplina(Codigo , Descricao) values ('UC06' , 'Planejar e executar a manutenção de redes locais de computadores');
INSERT INTO Disciplina(Codigo , Descricao) values ('UC07' , 'Planejar e executar a instalação, a configuração e o monitoramento de sistemas operacionais de redes locais (servidores)');
INSERT INTO Disciplina(Codigo , Descricao) values ('UC08' , 'Projeto Integrador Assistente de Operação de Redes de Computadores');
INSERT INTO Disciplina(Codigo , Descricao) values ('UC09' , 'Conceber, analisar e planejar o desenvolvimento de software');
INSERT INTO Disciplina(Codigo , Descricao) values ('UC10' , 'Executar os processos de codificação, manutenção e documentação de aplicativos computacionais para desktops');
INSERT INTO Disciplina(Codigo , Descricao) values ('UC11' , 'Executar os processos de codificação, manutenção e documentação de aplicativos computacionais para dispositivos móveis');
INSERT INTO Disciplina(Codigo , Descricao) values ('UC12' , 'Executar os processos de codificação, manutenção e documentação de aplicativos computacionais para internet');
INSERT INTO Disciplina(Codigo , Descricao) values ('UC13' , 'Executar teste e implantação de aplicativos computacionais');
INSERT INTO Disciplina(Codigo , Descricao) values ('UC14' , 'Desenvolver e organizar elementos estruturais de sites');
INSERT INTO Disciplina(Codigo , Descricao) values ('UC15' , 'Manipular e otimizar imagens vetoriais, bitmaps gráficos e elementos visuais de navegação para web');
INSERT INTO Disciplina(Codigo , Descricao) values ('UC16' , 'Projeto Integrador Assistente de Desenvolvimento de Aplicativos Computacionais');

drop table Disciplina; 