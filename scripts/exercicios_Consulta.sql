-- Liste todas as UCS junto com a chave primaria.
select * from disciplina;
desc disciplina ; 

select id  , concat(codigo , ' : '  , descricao ) as 'Unidade Curricular' from disciplina ; 

-- Liste todas as UCS em ordem inversa.
select id  , concat(codigo , ' : '  , descricao ) as 'Unidade Curricular'   from disciplina
order by Codigo desc ; 


-- Liste Qual o professor ministra a UC 5
select p.id , p.NOME as professor , d.Codigo as UC from disciplina d
inner join professor_disciplina pd
on d.ID = pd.ID_DISCIPLINA
inner join professor p 
on pd.ID_PROFESSOR = p.ID
where d.Codigo = 'UC05';

-- Liste a Diciplina que o professor ministra.

select * from professor ;

select * from disciplina ; 

select * from professor_disciplina ; 

select p.NOME as professor  , concat(d.Codigo , ' : ' , d.Descricao) as disciplina  from professor p 
inner join professor_disciplina pd on p.ID = pd.ID_PROFESSOR
inner join disciplina d  on pd.ID_DISCIPLINA = d.ID ; 

select p.NOME as professor  , concat(d.Codigo , ' : ' , d.Descricao) as disciplina 
from professor p 
inner join professor_disciplina pd on p.ID = pd.ID_PROFESSOR
inner join disciplina d  on pd.ID_DISCIPLINA = d.ID
order by 1 asc , 2 asc;

-- Liste a Quantidade de disciplina ministradas por professor .
select p.NOME as professor  , count(1) as 'UCS Ministradas'
from professor p 
inner join professor_disciplina pd  on p.id = pd.ID_PROFESSOR 
inner join disciplina d on pd.ID_DISCIPLINA = d.ID
group by professor ;

-- Liste o professor que mais ministra disciplinas.
select p.NOME as professor  , count(1) as UC
from professor p 
inner join professor_disciplina pd  on p.id = pd.ID_PROFESSOR 
inner join disciplina d on pd.ID_DISCIPLINA = d.ID
group by professor    ; 

select * from (
	select p.NOME as professor  , count(1) as UC
	from professor p 
	inner join professor_disciplina pd  on p.id = pd.ID_PROFESSOR 
	inner join disciplina d on pd.ID_DISCIPLINA = d.ID
	group by professor     
) x 
where x.uc =  (

		select max(x.uc) from (
			select count(1)  as UC
			from professor p 
			inner join professor_disciplina pd  on p.id = pd.ID_PROFESSOR 
			inner join disciplina d on pd.ID_DISCIPLINA = d.ID
		group by p.NOME ) as x 


 );


-- Liste O professor que menos ministra diciplinas.

select * from (
	select p.NOME as professor  , count(1) as UC
	from professor p 
	inner join professor_disciplina pd  on p.id = pd.ID_PROFESSOR 
	inner join disciplina d on pd.ID_DISCIPLINA = d.ID
	group by professor     
) x 
where x.uc = 
(
	select min(x.uc) from (
		select count(1)  as UC
		from professor p 
		inner join professor_disciplina pd  on p.id = pd.ID_PROFESSOR 
		inner join disciplina d on pd.ID_DISCIPLINA = d.ID
	group by p.NOME ) as x 
);
