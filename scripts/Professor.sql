CREATE TABLE IF NOT EXISTS Professor ( 
ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY  , 
NOME VARCHAR(200) NOT NULL 
) ;

INSERT INTO Professor(ID , NOME) values ( 1 , 'Daniel Santiago') ; 
INSERT INTO Professor(ID , NOME) values ( 2 , 'Thiago Portugal') ; 
INSERT INTO Professor(ID , NOME) values ( 3 , 'Marcelo Ferreira') ; 
INSERT INTO Professor(ID , NOME) values ( 4 , 'Marcia Fortuna') ; 
INSERT INTO Professor(ID , NOME) values ( 5 , 'Jonatan Silva') ; 
INSERT INTO Professor(ID , NOME) values ( 6 ,'Patrick Cansado') ; 


DROP TABLE Professor;


